$(document).ready(function(){
    //welcome-slider
    $('.welcome-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
    //
    $('.popular-slider').slick({
        variableWidth: true,
        slidesToShow: 1,
        arrows: false,
        dots: true
    });

    // header  menu
    $('.header__menu').click(function(e){
        e.preventDefault();

        $(this).toggleClass('header__menu--active');
        $('.header__nav-m').toggleClass('header__nav-m--active');
    });

    $(document).on('click',function (e){
        if(!$(e.target).closest('.header__menu,.header__nav-m').length) {
            $('.header__nav-m').removeClass('header__nav-m--active');
            $('.header__menu').removeClass('header__menu--active');
        }
    });
    // spiner
    $('.spiner').each(function (){
        var num = $(this).find('.spiner__num'),
            numT = parseInt(num.text()),
            down = $(this).find('.spiner__btn--down'),
            up = $(this).find('.spiner__btn--up'),
            dClass = 'spiner__btn--disable';

        if(numT < 1) {
            down.addClass(dClass);
        }else {
            down.removeClass(dClass);
        }

        down.click(function (e){
            e.preventDefault();
            numT = parseInt(num.text());

            if(numT >= 1) {
                num.text(parseInt(numT - 1));
                numT = parseInt(num.text());
            }else {
                num.text(0);
                numT = parseInt(num.text());
            }

            if(numT < 1) {
                down.addClass(dClass);
            }else {
                down.removeClass(dClass);
            }

        });

        up.click(function (e){
            e.preventDefault();
            numT = parseInt(num.text());
            num.text(parseInt(numT + 1));
            numT = parseInt(num.text());
            if(numT === 0) {
                down.addClass(dClass);
            }else {
                down.removeClass(dClass);
            }
        });
    });

    Inputmask("+7 99-999-99-99").mask(document.querySelectorAll(".phone-mask"));
    //
    $('.select-styler').select2({
        width: 'resolve',
        minimumResultsForSearch: -1
    });
    // header
    if($('.header').is('.header--scroll')) {
        $('body').addClass('body--scroll');
    }


    // Cache selectors
    var lastId,
        topMenu = $(".menu-nav"),
        topMenuHeight = topMenu.outerHeight()+70,
        // All list items
        menuItems = topMenu.find(".menu-nav__list-link"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
        $('html, body').stop().animate({ 
            scrollTop: offsetTop
        }, 300);
        e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
        // Get container scroll position
        var fromTop = $(this).scrollTop()+topMenuHeight;
        
        // Get id of current scroll item
        var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
            return this;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";
        
        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("menu-nav__list-item--active")
                .end().filter("[href='#"+id+"']").parent().addClass("menu-nav__list-item--active");

                
            var activePosition = $('.menu-nav__list-link').filter("[href='#"+id+"']").parent().position();
            
            $('.menu-nav__inner').stop().animate({scrollLeft:activePosition.left - 20}, 500, 'swing');
        }                   
    });
    // 

    var menuPosition = $('.menu-nav').offset(),
        headerHeigth = $('.header').outerHeight();
        
        if(window.innerWidth > 568) {
            var nScroll = 200;
        }else {
            var nScroll = 150;
        }
        
    $(window).resize(function(){
        menuPosition = $('.menu-nav').offset(),
        headerHeigth = $('.header').outerHeight();
    });

 
    // $(window).on('scroll', onScroll);

    // onScroll();

    function onScroll() {
        var st = $(window).scrollTop(),
        headerScroll = $('.header').is('.header--scroll');
    
        if(st > 50 && headerScroll) {
            $('.header--scroll').stop().addClass('header--shadow');
        }

        if(st < 50 && headerScroll) {
            $('.header--scroll').stop().removeClass('header--shadow'); 
        }

        if(st > menuPosition.top - headerHeigth && headerScroll) {
            $('.header--scroll').stop().removeClass('header--shadow').addClass('header--fixed');
            $('.menu-nav').stop().addClass('menu-nav--fixed');
        }

        if(st < menuPosition.top - headerHeigth && headerScroll) {
            $('.header--scroll').stop().removeClass('header--fixed');
            $('.menu-nav').stop().removeClass('menu-nav--fixed');
        }

        if(st > menuPosition.top + nScroll && headerScroll) {
            $('.menu-nav').stop().addClass('menu-nav--small');
        }

        if(st < menuPosition.top + nScroll && headerScroll) {
            $('.menu-nav').stop().removeClass('menu-nav--small');
        }
        pScroll();
        return; 
    }
    
    var headerScroll = $('.header').is('.header--scroll');

    if (headerScroll) {
        var $header = $( ".header" );         
        var $menu = $('.menu-nav');
        var appScroll = appScrollForward;
        var appScrollPosition = 0;
        var scheduledAnimationFrame = false;

        function appScrollReverse() {
            scheduledAnimationFrame = false;
            if ( appScrollPosition > 50 )
                return;
            $header.removeClass( "header--shadow" );
            appScroll = appScrollForward;
        }

        function appScrollForward() {
            scheduledAnimationFrame = false;
            if ( appScrollPosition < 50 )
                return;
            $header.addClass( "header--shadow" );
            appScroll = appScrollReverse;
        }

        function appScrollHandler() {
            appScrollPosition = window.pageYOffset;
            if ( scheduledAnimationFrame )
                return;
            scheduledAnimationFrame = true;
            requestAnimationFrame( appScroll );
        }

        $( window ).scroll( appScrollHandler );

        //        
        var appScroll2 = appScrollForward2;
        var scheduledAnimationFrame2 = false;
        var appScrollPosition2 = 0;
        var s = menuPosition.top - headerHeigth;

        function appScrollReverse2() {
            scheduledAnimationFrame2 = false;
            if ( appScrollPosition2 > s )
                return;
            $header.removeClass( "header--fixed" );
            $menu.removeClass( "menu-nav--fixed" );
            appScroll2 = appScrollForward2;
        }

        function appScrollForward2() {
            scheduledAnimationFrame2 = false;
            if ( appScrollPosition2 < s )
                return;
            $header.addClass( "header--fixed" ).removeClass( "header--shadow" );
            $menu.addClass( "menu-nav--fixed" );
            appScroll2 = appScrollReverse2;
        }

        function appScrollHandler2() {
            appScrollPosition2 = window.pageYOffset;
            if ( scheduledAnimationFrame2 )
                return;
            scheduledAnimationFrame2 = true;
            requestAnimationFrame( appScroll2 );
        }

        $( window ).scroll( appScrollHandler2 );
        //        
        var appScroll3 = appScrollForward3;
        var scheduledAnimationFrame3 = false;
        var appScrollPosition3 = 0;
        var d = menuPosition.top + nScroll;

        function appScrollReverse3() {
            scheduledAnimationFrame3 = false;
            if ( appScrollPosition3 > d )
                return;
            $menu.removeClass( "menu-nav--small" );
            appScroll3 = appScrollForward3;
        }

        function appScrollForward3() {
            scheduledAnimationFrame3 = false;
            if ( appScrollPosition3 < d )
                return;
            $menu.addClass( "menu-nav--small" );
            appScroll3 = appScrollReverse3;
        }

        function appScrollHandler3() {
            appScrollPosition3 = window.pageYOffset;
            if ( scheduledAnimationFrame3 )
                return;
            scheduledAnimationFrame3 = true;
            requestAnimationFrame( appScroll3 );
        }

        $( window ).scroll( appScrollHandler3 );
    }

    

    $('.menu-nav__list-link').click(function (e){
        e.preventDefault();
        var h= $(this).attr('href'),
            body = $("html, body"),
            p_os =  $(h).offset();
            if(window.innerWidth > 568) {
                var pt = p_os.top - 300;
            }else {
                var pt = p_os.top - 200;
            }
            

        body.stop().animate({scrollTop:pt}, 500, 'swing', function() {
        });
    });

    $('.menu-nav__mob-toggle').click(function (e){
        e.preventDefault();

        $(this).toggleClass('menu-nav__mob-toggle--active');
        $('.menu-nav__wrap').slideToggle();
    });
    // .address__edit-b-btn
    $('.address__edit-b-btn').click(function (e){
        e.preventDefault();
        $('.address__edit-b').not($(this).parents('.address__edit-b')).removeClass('address__edit-b--active');
        $(this).parents('.address__edit-b').toggleClass('address__edit-b--active');
    });

    $(document).on('click',function (e){
        if(!$(e.target).closest('.address__edit-b').length) {
            $('.address__edit-b').removeClass('address__edit-b--active');
        }
    });

    // cart aside
    if(window.innerWidth > 992 && $('.cart__aside').length) {
        var cart_position = $('.cart__aside').offset(),
            cart_h = $('.cart__cont').height(),
            cart_a_h = $('.cart__aside').height(),
            st = $(window).scrollTop(),
            c = parseInt((cart_position.top + cart_h) - cart_a_h)

        function cartAside() {
            if(st >= cart_position.top && st < c) {
                $('.cart__aside').addClass('cart__aside--fixed').css('left',cart_position.left);
            }else {
                $('.cart__aside').removeClass('cart__aside--fixed').attr('style','');
            }

            if( st >= c) {
                $('.cart__aside').addClass('cart__aside--absolute');
            }else {
                $('.cart__aside').removeClass('cart__aside--absolute');
            }
        }

        $(window).resize(function (){
            cart_position = $('.cart__aside').offset();
            cartAside();
        });

        cartAside();

        $(window).scroll(function(){
            st = $(window).scrollTop();

            cartAside();
        });
    }else {
        $('.cart__aside').removeClass('cart__aside--absolute cart__aside--fixed').attr('style','');
    }

    // pdp
    function pdp() {
        $('.pdp').each(function (){
            var asideH = $(this).find('.pdp__info').outerHeight();

            $(this).find('.pdp__inner').css('min-height', asideH);
        });
    }


    pdp();

    $(window).resize(function (){
        pdp();
    });

    // .pdp-slider
    $('.pdp-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });

    $('.pdp__zoom-btn').click(function (e){
        e.preventDefault();

        $('.pdp-modal').addClass('pdp-modal--active');
    });

    $('.pdp-modal__close').click(function (e){
        e.preventDefault();

        $('.pdp-modal').removeClass('pdp-modal--active');
    });
});

